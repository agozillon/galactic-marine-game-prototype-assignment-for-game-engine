#ifndef ABSTRACTCHARACTER_H
#define ABSTRACTCHARACTER_H
#include "abstractInGameObject.h"  // including the abstractInGameObject header so AbstractCharacter can inherit from it

// Abstract base class of character: no implementation as it is abstract and it inherits from AbstractInGameObject as it
// will be an in game object (thus needs draw etc.). This class is for characters and all characters be they NPC or PC will 
// require these functions
class AbstractCharacter: public AbstractInGameObject{
// all of the functions below are pure virtual functions so any class that inherits will need to implement its own or else be classed as abstract
public:
	virtual int getMaxHealth() = 0;                             // function to return the health value as an int.
	virtual int getStrength() = 0;                              // function to return the strength value as an int. 
	virtual int getSpeed() = 0;                                 // function to return the speed value as an int. 
	virtual void changeBaseStats(int str, int sp, int hp) = 0;  // function to change the characters basic stats without breaking OO(some characters may have other types of stats thats why its "Base")
	virtual ~AbstractCharacter(){}                              // blank deconstructor 
};

#endif