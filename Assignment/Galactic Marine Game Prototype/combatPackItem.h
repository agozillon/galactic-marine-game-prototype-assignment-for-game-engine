#ifndef COMBATPACKITEM_H
#define COMBATPACKITEM_H
#include "abstractItem.h"  // including abstractItem so that CombatPackItem can inherit from it and gain libraries from it

// as you can see below CombatPackItem is a culmination of all of the inherited Abstract classes functions
class CombatPackItem: public AbstractItem{

public:
	CombatPackItem();                                      // CombatPackItem constructor
	void drawLabel(float x, float y);                      // function for taking in two float variables and then drawing the label at that position 
	void draw();                                           // function for calling the relevant draw functions to draw the object on screen
	void updatePosition(float xPos, float yPos);           // function for updating the objects position (just the rectangle in this case)
	int getStatIncrease(){return strengthIncrease;}        // inline function that returns the CombatPackItem's strength increase
	void resetVariables();                                 // function for reseting the variables of CombatPackItem (mostly the visible boolean but in future there could be more) 
	bool getStateOfVisibility(){return visible;}            // inline function that returns the state of the visible boolean
	void changeBoolVisible(bool change){visible = change;} // function for changing the visible boolean
	~CombatPackItem();                                     // CombatPackItem destructor

private:
	// variables and/or object pointers that CombatPackItem requires the identifiers are descriptive
	int strengthIncrease;
	bool visible;
};

#endif