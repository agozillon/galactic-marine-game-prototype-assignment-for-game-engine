#ifndef ABSTRACTINGAMEOBJECT_H
#define ABSTRACTINGAMEOBJECT_H
#include "rect.h" // including rect header and label header so that we can create pointers for these classes
#include "label.h"
#include <SDL.h> // including SDL library here so all derived objects have SDL(I Know label and rect give 
                 //SDL libraries but it doesn't make it obvious that I wish to have SDL libraries for this hierarchy

// Abstract base class for In Game Objects(Objects that will be drawn within the game): no implementation as it is an
// abstract class and all in game objects that shall be interacted with shall inherit from this and will require the following
// functions and objects 
class AbstractInGameObject{
// all of the functions below are pure virtual functions so any class that inherits will need to implement its own or else be classed as abstract
public:
	virtual void draw() = 0;                                 // function to draw the objects within the game.  
	virtual void updatePosition(float xPos, float yPos) = 0; // function used to update the position of the object on screen. 
	virtual bool getStateOfVisibility() = 0;                  // function used to return the current value of the visibility boolean
	virtual void changeBoolVisible(bool change) = 0;         // function used to change the current value of the visibility boolean
	virtual void resetVariables() = 0;                       // function used to reset all the variables of the object
	virtual void drawLabel(float x, float y) = 0;            // function used to draw the objects label and input the position the label should be
	virtual ~AbstractInGameObject(){}                        // blank deconstructor 

	// rect and label object pointers, one used to draw the object(Which will be represented by a rectangle) the other to draw the name of the object 
	Rect * marker;
	Label * name;
};

#endif