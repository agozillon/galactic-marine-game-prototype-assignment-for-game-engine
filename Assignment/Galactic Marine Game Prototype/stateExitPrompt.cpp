#include "stateExitPrompt.h" // including the stateExitPrompt header so I can code implementations of the functions
#include "game.h"       // including game so I can call various Game functions (like getSDLWindow, or state related functions)

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StateExitPrompt::StateExitPrompt()
{
	init();
}

// function to initilize the various objects and variables within this state
void StateExitPrompt::init()
{
	// creating Labels
	SDL_Color colour = { 255, 255, 0 };
	exitPromptLabel = new Label("Would you like to save the game in play? y/n", TTF_OpenFont("MavenPro-Regular.ttf", 24),  colour);
}

// function to draw all of the various things that are required to be drawn during this state
void StateExitPrompt::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
  
	//drawing labels to screen
	exitPromptLabel->draw(-0.15, 0.1);

	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StateExitPrompt::update()
{ 
	draw();
}

// function for handling various keyboard input and changing/effecting the current state depending on choice
void StateExitPrompt::handleSDLEvent(SDL_Event const &sdlEvent)
{
  if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{		

			// if Y/y key is pressed then save the last played game and then quit the game 
				case 'Y': case 'y': 
					Game::getInstance()->getCastedStatePlay()->save();

					SDL_Event quitEvent;
					quitEvent.type = SDL_QUIT;
					SDL_PushEvent(&quitEvent);
					break;
			
			// if N/n is pressed then quit the game
				case 'N': case 'n':
					quitEvent.type = SDL_QUIT;
					SDL_PushEvent(&quitEvent);
					break;

			// if any other key is pressed do nothing
				default:
					break;
		}
	}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StateExitPrompt::exit()
{
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StateExitPrompt::enter()
{

}

// StateExitPrompt destructor deleting various objects that get instantiated within this class
StateExitPrompt::~StateExitPrompt()
{
	// deleting any objects I instantiated within this class
	delete exitPromptLabel;
}