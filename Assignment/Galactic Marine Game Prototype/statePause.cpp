#include "statePause.h" // including the statePause header so I can code implementations of the functions
#include "game.h"       // including game so I can call various Game functions (like getSDLWindow, or state related functions)

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StatePause::StatePause()
{
	init();
}

// function to initilize the various objects and variables within this state
void StatePause::init()
{
	// creating Labels
	SDL_Color colour = { 255, 255, 0 };
	pauseLabel = new Label("Game Is Currently Paused", TTF_OpenFont("MavenPro-Regular.ttf", 24),  colour);
	pauseDescriptionLabel = new Label("To resume the game press escape,  exit to the menu press Y", TTF_OpenFont("MavenPro-Regular.ttf", 15),  colour);
}

// function to draw all of the various things that are required to be drawn during this state
void StatePause::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
  
	//drawing labels to screen
	pauseLabel->draw(-0.15, 0.1);
	pauseDescriptionLabel->draw(-0.3, 0);

	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StatePause::update()
{ 
	draw();
	
}

// function for handling various keyboard input and changing/effecting the current state depending on choice
void StatePause::handleSDLEvent(SDL_Event const &sdlEvent)
{
  if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{		
				case 'Y': case 'y': 
					Game::getInstance()->changeContinueGame(true);
					Game::getInstance()->setState(Game::getInstance()->getMainMenuState());
					break;

				case SDLK_ESCAPE:
					Game::getInstance()->setState(Game::getInstance()->getPlayState());
					break;

				default:
					break;
		}
	}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StatePause::exit()
{
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StatePause::enter()
{

}

// statePause destructor deleting various objects that get instantiated within this class
StatePause::~StatePause()
{
	// deleting any objects I instantiated within this class
	delete pauseLabel;
	delete pauseDescriptionLabel;

}