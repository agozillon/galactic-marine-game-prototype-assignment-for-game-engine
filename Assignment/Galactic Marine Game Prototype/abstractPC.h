#ifndef ABSTRACTPC_H
#define ABSTRACTPC_H
#include "abstractCharacter.h" // including the abstractCharacter header so AbstractPC can inherit from it

// Abstract base class for PC's encase they have seperate functions from NPC(player character) and since we have
// a character select screen there will no doubt be multiple implementations of PC(for seperate classes): no 
//implementation as it is abstract and it inherits from AbstractInGameObject as it will be an in game object 
//(thus needs draw etc.). this class is specificly for in game PCs and thus all in game PCS will require these 
//functions.
class AbstractPC: public AbstractCharacter{
// all of the functions below are pure virtual functions so any class that inherits will need to implement its own or else be classed as abstract
public:
	virtual int getCurrentMoney() = 0;                                                 // function to return the amount of money that each NPC will drop
	virtual void changeCurrentMoney(int m) = 0;                                        // function to change the players money
	virtual int getCurrentHealth() = 0;                                                // function to return the current health
	virtual void changeCurrentHealth(int hp) = 0;                                      // function to change the players current health
	virtual int getNumberOfEnemiesKilled() = 0;                                        // function to get the number of enemies killed by the player
	virtual void changeNumberOfEnemiesKilled(int amountKilled) = 0;                    // function to change the number of enemies killed
    virtual void setStats(int str, int sp, int chp, int mhp, int cm, int ek) = 0;      // function to set the stats, difference from changeStat function is that this one is for setting entirely new stats by using the =, rather than editing pre-existing stats as well as setting currentMoney and enemies killed
	virtual ~AbstractPC(){}                                                            // blank deconstructor
};

#endif