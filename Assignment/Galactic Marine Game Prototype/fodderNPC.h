#ifndef FODDERNPC_H
#define FODDERNPC_H
#include "abstractNPC.h" // including abstractNPC so that FodderNPC can inherit from it and gain libraries from it 

// as you can see below FodderNPC is a culmination of all of the inherited Abstract classes functions
class FodderNPC: public AbstractNPC{

public:
	FodderNPC();                                           // FodderNPC constructor  
	void drawLabel(float x, float y);                      // function for taking in two float variables and then drawing the label at that position 
	void draw();                                           // function for calling the relevant draw functions to draw the object on screen
	void updatePosition(float xPos, float yPos);           // function for updating the objects position (just the rectangle in this case)
	int getStrength(){return strength;}                    // inline function that returns the FodderNPCs strength
	int getMaxHealth(){return maxHealth;}                  // inline function that returns the FodderNPCs maxHealth(maximum health)
	int getSpeed(){return speed;}                          // inline function that returns the FodderNPCs speed
	int getMoneyDrop(){return moneyDrop;}                  // inline function that returns the amount of money the FodderNPCs drop 
	int getDropRate(){return dropRate;}                    // inline function that returns the item drop rate for the FodderNPCs
	void changeBaseStats(int str, int sp, int hp);         // function for changing the base stats of FodderNPC
	void resetVariables();                                 // function for reseting the variables of FodderNPC 
	bool getStateOfVisibility(){return visible;}           // inline function that returns the state of the visible boolean
	void changeBoolVisible(bool change){visible = change;} // inline function that changes the visible bool by passing in the change parameter 
	~FodderNPC();                                          // FodderNPC destructor  


private:
	// variables and/or object pointers that FodderNPC requires the identifiers are descriptive
	int maxHealth;
	int strength; 
	int speed;
	int moneyDrop;
	int dropRate;
	bool visible;
};

#endif