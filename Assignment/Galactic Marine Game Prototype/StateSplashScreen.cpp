#include "StateSplashScreen.h" // including the stateSplashScreen header so I can code implementations of the functions
#include "game.h"              // including game so I can call various Game functions (like getSDLWindow, or state related functions)
#include <ctime>               // C time libraries for rand and time functions

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StateSplashScreen::StateSplashScreen()
{	
	init();
}

// function to initilize the various objects and variables within this state
void StateSplashScreen::init()
{
	// taking the time of entry to the state in seconds
	entryTime = clock() / CLOCKS_PER_SEC; 

	// creating labels
	SDL_Color colour = { 255, 255, 0 };
	developerLogoLabel = new Label("UWS Tech", TTF_OpenFont("MavenPro-Regular.ttf", 80), colour);
}

// function to draw all of the various things that are required to be drawn during this state
void StateSplashScreen::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	//drawing labels to screen
	developerLogoLabel->draw(-0.3, 0);

	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StateSplashScreen::update()
{ 
	draw();
	// creating temporary float variables to hold the clock ticks and to set the exit time of this state
	float updatingTime;
	float timeInSplash;
	float exitTimeOfSplash = 3;

	// constantly updating the time within the state
	updatingTime = clock() / CLOCKS_PER_SEC; 
	
	// taking away the entry time from the updated time to calculate time spent in this state
	timeInSplash = updatingTime - entryTime; 

	// if the time spent in this state is greater than exitTimeOfSplash set the state back to main menu
	if(timeInSplash > exitTimeOfSplash) 
	{
		Game::getInstance()->setState(Game::getInstance()->getMainMenuState());
	}
	
}

// function for handling various keyboard input and changing/effecting the current state depending on choice
void StateSplashScreen::handleSDLEvent(SDL_Event const &sdlEvent)
{
  if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{

			// if return key or space key is hit change the state to Main Menu state 
			case SDLK_RETURN: case SDLK_SPACE:
				Game::getInstance()->setState(Game::getInstance()->getMainMenuState());
				break;

		    // if any other key is pressed don't do anything
			default:
				break;

		}
	}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StateSplashScreen::exit()
{
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StateSplashScreen::enter()
{
	// taking the time of entry to the state in seconds
	entryTime = clock() / CLOCKS_PER_SEC; 
}

// stateSplashScreen destructor deleting various objects that get instantiated within this class
StateSplashScreen::~StateSplashScreen()
{
	// deleting instantiated objects
	delete developerLogoLabel; 
}