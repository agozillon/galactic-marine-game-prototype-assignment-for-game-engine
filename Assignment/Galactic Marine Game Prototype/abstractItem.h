#ifndef ABSTRACTITEM_H
#define ABSTRACTITEM_H
#include "abstractInGameObject.h"  // including the abstractInGameObject header so AbstractItem can inherit from it

// Abstract base class for Items: no implementation as it is abstract and it inherits from AbstractInGameObject as it
// will be an in game object (thus needs draw etc.). this class is specificly for in game items and thus all in game items
// will require these functions. (it's currently quite blank but it leaves items open for extension in the future)
class AbstractItem: public AbstractInGameObject{
// all of the functions below are pure virtual functions so any class that inherits will need to implement its own or else be classed as abstract
public:
	virtual int getStatIncrease() = 0; // function to get the Stat increase for each item
	virtual ~AbstractItem(){}          // blank deconstructor 
};

#endif