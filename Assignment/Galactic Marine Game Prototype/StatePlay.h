#ifndef STATEPLAY_H
#define STATEPLAY_H
#include "gameState.h" // including gameState so statePlay can inherit from it 
#include <SDL_ttf.h>   // including SDL and GL libraries so that I can use various functions and types within my class
#include <SDL.h>
#include <ctime>       // C time libraries for rand and time functions
#include <iostream>    // input output stream and  for various functions to output values                
#include <sstream>     // stringstream to help stream strings into labels
#include "label.h"     // including label/rect and AbstractPC/NPC/Item so I can create pointers and objects of these types
#include "rect.h"
#include "abstractPC.h"
#include "abstractNPC.h"
#include "abstractItem.h"

// as you can see below StatePlay is a culmination of all of the inherited Abstract classes functions with some extension
class StatePlay: public GameState{

public:
	StatePlay();                                       // StatePlay constructor
    void draw();                                       // Function that is used to draw collections of other objects during this state
    void init();                                       // Function that is used to initilize all of the things that are required to run this state(instead of using the constructor)
    void update();                                     // Function that is used to update this state 
	void load();                                       // Function that is used to load the previously saved game
	void save();                                       // Function that is used to save the current game
    void handleSDLEvent(SDL_Event const &sdlEvent);    // Function that is used to handleSDLEvents, all keyboard events at the moment
    void exit();                                       // Function that should be called when the state is switching, anything you wish to happen when you exit this state should go in this function
    void enter();                                      // Function that should be called when the state is switching, anything you wish to happen when you enter this state should go in this function
	~StatePlay();                                      // StatePlay destructor


private:
	void mapOne();                                     // Function that resets the variables and holds all of the positions of all the items and characters in "mapOne", other "map" functions could be added later however I'd probably try create levels/maps differently
	
	// other variables and/or object pointers that StatePlay requires the identifiers are descriptive
	// I shall explain the use for some of the ones thats usage may seem slightly unusual
	AbstractPC * player;
	AbstractNPC * fodderEnemies[5];
	AbstractNPC * bruteEnemies[3];
	AbstractNPC * raiderEnemies;
	AbstractItem * healthPack[2];
	AbstractItem * combatPack[2];
	AbstractItem * stimulant;
	Label * playerStatLabel;

	// I use these to track time between frames
	clock_t lastTime; // clock_t is an integer type
	clock_t currentTime;

	// the below ints are values to keep track of the number of specific objects in arrays
	int numberOfCombatPacks;
	int numberOfHealthPacks; 
	int numberOfFodderEnemies;
	int numberOfBruteEnemies;
	int numberOfRaiderEnemies;
	int totalEnemiesInGame; 
};

#endif