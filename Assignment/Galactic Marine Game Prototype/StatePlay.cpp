#include<fstream>       // including file stream so I can save and load in game "save files"
#include "statePlay.h"  // including the statePause header so I can code implementations of the functions
#include "game.h"       // including game so I can call various Game functions (like getSDLWindow, or state related functions)
#include "originalPC.h" // including all of the PC/NPCs/Items that are required so that i can instantiate them and use there functions to create the game
#include "bruteNPC.h"
#include "fodderNPC.h"
#include "raiderNPC.h"
#include "combatPackItem.h"
#include "healthPackItem.h"
#include "stimulantItem.h"
using namespace std;

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StatePlay::StatePlay()
{
	init();
}

// The event handling function
// In principle, this function should never perform updates to the game
// ONLY detect what events have taken place. Reacting to the events should 
// be taken care of in a seperate update function
// This would allow e.g. diagonal movement when two keys are pressed together
// (which is not possible with this implementation)
void StatePlay::handleSDLEvent(SDL_Event const &sdlEvent)
{
	
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{
			
			// if the up or W/w key are pressed then move the player positively on the Y (upwards)
			case SDLK_UP:
			case 'w': case 'W': 
				player->updatePosition(player->marker->getX(), player->marker->getY() + 0.05f);
				break;
			
			// if the down or S/s key are pressed then move the player negatively on the Y (downwards)
			case SDLK_DOWN:
			case 's': case 'S':
				player->updatePosition(player->marker->getX(), player->marker->getY() - 0.05f);
				break;
        
			// if the left or A/a key are pressed then move the player negatively on the X (left)
			case SDLK_LEFT:
			case 'a': case 'A': 
				player->updatePosition(player->marker->getX() - 0.05f, player->marker->getY());
				break;
           
			// if the right or D/d key are pressed then move the player positively on the X (right)
			case SDLK_RIGHT:
			case 'd': case 'D':
				player->updatePosition(player->marker->getX() + 0.05f, player->marker->getY());
				break;
		  
			// if the escape key is pressed then set the state to the Pause State
			case SDLK_ESCAPE:
				Game::getInstance()->setState(Game::getInstance()->getPauseState());
				break;
	   
			// if any other keys are pressed do nothing
			default:
				break;

		}
	}
}

// Initialise OpenGL values and game related values and variables
void StatePlay::init() 
{
	// setting up random and lastTime for the fps
	std::srand( std::time(NULL) );
	lastTime = clock();
	

	// setting the number of each type of enemy and items that are in the game 
	//(unless there is only one of them in the game like the raider or stimulant) 
	//this is to keep count of the enemies and items within the game for loops and such!
	numberOfFodderEnemies = 5;
	numberOfBruteEnemies = 3;
	numberOfHealthPacks = 2;
	numberOfCombatPacks = 2;

	// working out the total number of enemies in the game
	totalEnemiesInGame = numberOfFodderEnemies+numberOfBruteEnemies+1;

	//creating all of the objects within the game such as the player, enemies, items
	// and labels
	player = new OriginalPC();

	for(int i = 0; i < numberOfFodderEnemies; i++)
	{
		fodderEnemies[i] = new FodderNPC();
	}

	for(int i = 0; i < numberOfBruteEnemies; i++)
	{
		bruteEnemies[i] = new BruteNPC();
	}

	raiderEnemies = new RaiderNPC();

	for(int i = 0; i < numberOfHealthPacks; i++)
	{
		healthPack[i] = new HealthPackItem();
	}

	for(int i = 0; i < numberOfCombatPacks; i++)
	{
		combatPack[i] = new CombatPackItem();
	}

	stimulant = new StimulantItem();
	
	playerStatLabel = new Label();

	// have to initilize the label with a new OpenFont, colour and size via this constructor
	// otherwise in release mode we get a strange graphical artefact of the streamed stats carrying 
	// over to the Credits (R) label and replacing it. As the release version finds the blank
	// label constructor ambigious. Alternative is to change playerStatLabel to "Credits (R)" 
	// on exit but...takes up more performance than just once on initilization
	SDL_Color colour = { 255, 255, 0 };
	playerStatLabel = new Label("initilizing", TTF_OpenFont("MavenPro-Regular.ttf", 24), colour);
}


// The main rendering function
// In principle, this function should never perform updates to the game
// ONLY render the current state. Reacting to events should be taken care
// of in a seperate update function
void StatePlay::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	
	// draw player if visible true
	if(player->getStateOfVisibility())
		player->draw();

	//draw enemies if visible true
	for(int i = 0; i < numberOfFodderEnemies; i++)
	{
		if(fodderEnemies[i]->getStateOfVisibility())
			fodderEnemies[i]->draw();
	}

	for(int i = 0; i < numberOfBruteEnemies; i++)
	{
		if(bruteEnemies[i]->getStateOfVisibility())
			bruteEnemies[i]->draw();
	}

	if(raiderEnemies->getStateOfVisibility())
		raiderEnemies->draw();

	//drawing various items such as Stimulants, Health Packs and Combat Packs if they're visible
	for(int i = 0; i < numberOfHealthPacks; i++)
	{
		if(healthPack[i]->getStateOfVisibility())
			healthPack[i]->draw();
	}
  
	for(int i = 0; i < numberOfCombatPacks; i++)
	{
		if(combatPack[i]->getStateOfVisibility())
			combatPack[i]->draw();
	}

	if(stimulant->getStateOfVisibility())
		stimulant->draw();
    
	// Calculate ms/frame
    // Some OpenGL drivers will limit the frames to 60fps (16.66 ms/frame)
    // If so, expect to see the time to rapidly switch between 16 and 17...
    // On some systems, CLOCKS_PER_SECOND is 1000, which makes the arithmetic below redundant
    // - but this is not necessarily the case on all systems
	currentTime = clock();
    float milliSecondsPerFrame = ((currentTime - lastTime)/(float)CLOCKS_PER_SEC*1000);
	lastTime = clock();
 
	// Print out the players statistics and the frame time information
    std::stringstream strStream;
	strStream << "  ms/frame: " << milliSecondsPerFrame;
	strStream << " Player Statistics:";
 	strStream << "  Health: " << player->getCurrentHealth();
 	strStream << "  Strength: " << player->getStrength();
	strStream << "  Speed: " << player->getSpeed();
	strStream << "  Money: " << player->getCurrentMoney();

    
	// draw player statistics and milliSecondsPerFrame
	playerStatLabel->textToTexture(strStream.str().c_str(), TTF_OpenFont("MavenPro-Regular.ttf", 24));
    playerStatLabel->draw(-0.9, -0.9);
	
	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StatePlay::update()
{
	draw();
	
	// for loop that checks all of games fodder enemies and checks if they are visible and if a collision is occuring if they're true then set the currentEnemy to that NPC and change the state to combat state
	for(int i = 0; i < numberOfFodderEnemies; i++)
	{
		if	( (fodderEnemies[i]->marker->getX() >= player->marker->getX()) && (fodderEnemies[i]->marker->getX() +fodderEnemies[i]->marker->getW() <= player->marker->getX()+player->marker->getW()) 	// cursor surrounds target in x
			&& (fodderEnemies[i]->marker->getY()  >= player->marker->getY()) && (fodderEnemies[i]->marker->getY()+fodderEnemies[i]->marker->getH() <= player->marker->getY()+player->marker->getH())) // cursor surrounds target in y
	    {
			if(fodderEnemies[i]->getStateOfVisibility() && player->getStateOfVisibility())
			{
				Game::getInstance()->setCurrentEnemy(fodderEnemies[i]);
				Game::getInstance()->setState(Game::getInstance()->getCombatState());
			}
		}
	}

	// for loop that checks all of games brute enemies and checks if they are visible and if a collision is occuring if they're true then set the currentEnemy to that NPC and change the state to combat state
	for(int i = 0; i < numberOfBruteEnemies; i++)
	{
		if	( (bruteEnemies[i]->marker->getX() >= player->marker->getX()) && (bruteEnemies[i]->marker->getX() +bruteEnemies[i]->marker->getW() <= player->marker->getX()+player->marker->getW()) 	// cursor surrounds target in x
			&& (bruteEnemies[i]->marker->getY()  >= player->marker->getY()) && (bruteEnemies[i]->marker->getY()+bruteEnemies[i]->marker->getH() <= player->marker->getY()+player->marker->getH() ) ) // cursor surrounds target in y
	    {
			if(bruteEnemies[i]->getStateOfVisibility() && player->getStateOfVisibility())
			{
				Game::getInstance()->setCurrentEnemy(bruteEnemies[i]);
				Game::getInstance()->setState(Game::getInstance()->getCombatState());
			}
		}	
	}

	//  checks the raider enemie and check if hes visible and if a collision is occuring if they're true then set the currentEnemy to that NPC and change the state to combat state
	if ( (raiderEnemies->marker->getX() >= player->marker->getX()) && (raiderEnemies->marker->getX() +raiderEnemies->marker->getW() <= player->marker->getX()+player->marker->getW()) 	// cursor surrounds target in x
	   	&& (raiderEnemies->marker->getY()  >= player->marker->getY()) && (raiderEnemies->marker->getY()+raiderEnemies->marker->getH() <= player->marker->getY()+player->marker->getH() ) ) // cursor surrounds target in y
	{
		if(raiderEnemies->getStateOfVisibility() && player->getStateOfVisibility())
		{
			Game::getInstance()->setCurrentEnemy(raiderEnemies);
		    Game::getInstance()->setState(Game::getInstance()->getCombatState());
		}
	}

	// for loop that checks all of the games combatPack items and checks if they are visible and if a collision is occuring if they're true then change the players stats based on the items getStatIncrease() function and then set the items visible to false
	for(int i = 0; i < numberOfCombatPacks; i++)
	{
		if	( (combatPack[i]->marker->getX() >= player->marker->getX()) && (combatPack[i]->marker->getX() +combatPack[i]->marker->getW() <= player->marker->getX()+player->marker->getW()) 	// cursor surrounds target in x
			&& (combatPack[i]->marker->getY()  >= player->marker->getY()) && (combatPack[i]->marker->getY()+combatPack[i]->marker->getH() <= player->marker->getY()+player->marker->getH() ) ) // cursor surrounds target in y
	    {
			if(combatPack[i]->getStateOfVisibility() && player->getStateOfVisibility())
			{
				player->changeBaseStats(combatPack[i]->getStatIncrease(), 0, 0); 
				combatPack[i]->changeBoolVisible(false);
			}
		}
	}

	// for loop that checks all of the games healthPack items and checks if they are visible and if a collision is occuring if they're true then change the players stats based on the items getStatIncrease() function and then set the items visible to false
	for(int i = 0; i < numberOfHealthPacks; i++)
	{
		if	( (healthPack[i]->marker->getX() >= player->marker->getX()) && (healthPack[i]->marker->getX() +healthPack[i]->marker->getW() <= player->marker->getX()+player->marker->getW()) 	// cursor surrounds target in x
			&& (healthPack[i]->marker->getY()  >= player->marker->getY()) && (healthPack[i]->marker->getY()+healthPack[i]->marker->getH() <= player->marker->getY()+player->marker->getH() ) ) // cursor surrounds target in y
	    {
			if(healthPack[i]->getStateOfVisibility() && player->getStateOfVisibility())
			{
				player->changeBaseStats(0, 0, healthPack[i]->getStatIncrease()); 
				healthPack[i]->changeBoolVisible(false);
			}
		}
	}

	// checks the games stimulant item and checks if they are visible and if a collision is occuring if they're true then change the players stats based on the items getStatIncrease() function and then set the items visible to false
	if ( (stimulant->marker->getX() >= player->marker->getX()) && (stimulant->marker->getX() +stimulant->marker->getW() <= player->marker->getX()+player->marker->getW()) 	// cursor surrounds target in x
		  && (stimulant->marker->getY()  >= player->marker->getY()) && (stimulant->marker->getY()+stimulant->marker->getH() <= player->marker->getY()+player->marker->getH() ) ) // cursor surrounds target in y
	    {
			if(stimulant->getStateOfVisibility() && player->getStateOfVisibility())
			{
				player->changeBaseStats(0, stimulant->getStatIncrease(), 0);
				stimulant->changeBoolVisible(false);
			}
		}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StatePlay::exit()
{	
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StatePlay::enter()
{
	glClearColor(0.0, 0.0, 0.0, 0.0); // set background colour

	// set the current player pointer to the object player which is an OriginalPC type
	// this is largely for use within the stateCombat
	Game::getInstance()->setCurrentPlayer(player);
	
	// if continueGameBool is true then change it to false(this also doesn't call the reset function and loads in a saved game if the isThereASavedGame bool is set to true
	if(Game::getInstance()->getContinueGameBool())
	{
		if(Game::getInstance()->getIsThereASavedGame())
		{
			load();
		}
		Game::getInstance()->changeContinueGame(false);
	}
	
	// if newGameBool is true then call the mapOne() function(basically a reset function) and change newGame to false
	if(Game::getInstance()->getNewGameBool())
	{
		mapOne();
		Game::getInstance()->changeNewGame(false);
	}
	
	// if player visibility is false then set the state to the GameOverState
	if(player->getStateOfVisibility() == false)
	{
		Game::getInstance()->setState(Game::getInstance()->getGameOverState());
	}

	// if player number Of Enemies Killed is equal to the total enemies in the game then set the state to the GameWonState
	if(player->getNumberOfEnemiesKilled() == totalEnemiesInGame)
	{
		Game::getInstance()->setState(Game::getInstance()->getGameWonState());
	}
}

// function that is used to save all of the player/enemy/item variables that are required to keep track of a game in plays state
// onto a file
void StatePlay::save()
{

 // create a output file stream variable and then open the file saveFile.txt with it
  std::ofstream saveFile; 
  saveFile.open ("saveFile.txt");
  	
  // if the file is open then start the saving process else output the error message
  if (saveFile.is_open())
	{  
		// saving the players various stats
		saveFile << player->getMaxHealth() << "\n";
		saveFile << player->getCurrentHealth() << "\n";
		saveFile << player->getSpeed() << "\n";
		saveFile << player->getStrength() << "\n";
		saveFile << player->getCurrentMoney() << "\n";
		saveFile << player->getNumberOfEnemiesKilled() << "\n";
		saveFile << player->getStateOfVisibility() << "\n";
		saveFile << player->marker->getX() << "\n";
		saveFile << player->marker->getY() << "\n";
  
		// saving the stimulants various stats
		saveFile << stimulant->getStateOfVisibility() << "\n";
		saveFile << stimulant->marker->getX() << "\n";
		saveFile << stimulant->marker->getY() << "\n";
  
		// saving the health packs various stats
	    for(int i = 0; i < numberOfHealthPacks; i++)
		{
			saveFile << healthPack[i]->getStateOfVisibility() << "\n";
			saveFile << healthPack[i]->marker->getX() << "\n";
			saveFile << healthPack[i]->marker->getY() << "\n";
		}

		// saving the combat packs various stats
		for(int i = 0; i < numberOfCombatPacks; i++)
	    {
			saveFile << combatPack[i]->getStateOfVisibility() << "\n";
			saveFile << combatPack[i]->marker->getX() << "\n";
			saveFile << combatPack[i]->marker->getY() << "\n";
		}

		// saving the fodder enemies various stats
	    for(int i = 0; i < numberOfFodderEnemies; i++)
		{
		   saveFile << fodderEnemies[i]->getStateOfVisibility() << "\n";
		   saveFile << fodderEnemies[i]->marker->getX() << "\n";
		   saveFile << fodderEnemies[i]->marker->getY() << "\n";
		}

		// saving the brute enemies various stats
	    for(int i = 0; i < numberOfBruteEnemies; i++)
		{
		  saveFile << bruteEnemies[i]->getStateOfVisibility() << "\n";
		  saveFile << bruteEnemies[i]->marker->getX() << "\n";
		  saveFile << bruteEnemies[i]->marker->getY() << "\n";
	    }

	// saving the raider enemies various stats
	 saveFile << raiderEnemies->getStateOfVisibility() << "\n";
	 saveFile << raiderEnemies->marker->getX() << "\n";
	 saveFile << raiderEnemies->marker->getY() << "\n";
  }
  else std::cout << "Unable to open file";

  // closing the file so other streams can get access to it and the file doesn't get accidently damaged
  saveFile.close();
}

// function used to load in a previously saved game from a file, it is slightly unwieldly at the moment i feel however
// this is the simplest way I could think of and it's my first time trying this. And since it's a set data structure 
// it works
void StatePlay::load()
{
	// creating function scope variables to help with the movement and conversion of data
	string loadedVariable;
	int maxHealth, currentHealth, strength, speed, currentMoney, numberOfEnemiesKilled, stateOfVisibility, boolState;
	float xPos, yPos;

	// create a input file stream variable and then open the file saveFile.txt with it
	std::ifstream readSaveFile;
	readSaveFile.open("saveFile.txt");
  
	// if the file is open then start the loading process else output the error message
	if (readSaveFile.is_open())
	{  
			// getting in the players state, position, stats, and enemieskilled from the file and then setting them
			getline(readSaveFile, loadedVariable);
			maxHealth = atoi(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			currentHealth = atoi(loadedVariable.c_str());
			
			getline(readSaveFile, loadedVariable);
			speed = atoi(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			strength = atoi(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			currentMoney = atoi(loadedVariable.c_str());
			
			getline(readSaveFile, loadedVariable);
			numberOfEnemiesKilled = atoi(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			boolState = atoi(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			xPos = atof(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			yPos = atof(loadedVariable.c_str());

			player->setStats(strength, speed, currentHealth, maxHealth, currentMoney, numberOfEnemiesKilled);
			player->changeBoolVisible(boolState);
			player->updatePosition(xPos, yPos);
			
			// getting in stimulants state and position from the file and then setting them
			getline(readSaveFile, loadedVariable);
			boolState = atoi(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			xPos = atof(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			yPos = atof(loadedVariable.c_str());
			
			stimulant->changeBoolVisible(boolState);
			stimulant->updatePosition(xPos, yPos);

			// getting in healthPack 1s state and position from the file and then setting them
			for(int i = 0; i < numberOfHealthPacks; i++)
			{
				getline(readSaveFile, loadedVariable);
				boolState = atoi(loadedVariable.c_str());

				getline(readSaveFile, loadedVariable);
				xPos = atof(loadedVariable.c_str());
	
				getline(readSaveFile, loadedVariable);
				yPos = atof(loadedVariable.c_str());
			
				healthPack[i]->changeBoolVisible(boolState);
				healthPack[i]->updatePosition(xPos, yPos);
			}

			// getting in combatPack's states and position from the file and then setting them
			for(int i = 0; i < numberOfCombatPacks; i++)
			{
				// getting in combatPack's states and position from the file and then setting them
				getline(readSaveFile, loadedVariable);
				boolState = atoi(loadedVariable.c_str());

				getline(readSaveFile, loadedVariable);
				xPos = atof(loadedVariable.c_str());

				getline(readSaveFile, loadedVariable);
				yPos = atof(loadedVariable.c_str());
			
				combatPack[i]->changeBoolVisible(boolState);
				combatPack[i]->updatePosition(xPos, yPos);
			}

			// getting in fodderEnemies states , health and position from the file and then setting them
			for(int i = 0; i < numberOfFodderEnemies; i++)
			{
				getline(readSaveFile, loadedVariable);
				boolState = atoi(loadedVariable.c_str());

				getline(readSaveFile, loadedVariable);
				xPos = atof(loadedVariable.c_str());

				getline(readSaveFile, loadedVariable);
				yPos = atof(loadedVariable.c_str());
			
				fodderEnemies[i]->changeBoolVisible(boolState);
				fodderEnemies[i]->updatePosition(xPos, yPos);
			}

	        // getting in bruteEnemies states , health and position from the file and then setting them
			for(int i = 0; i < numberOfBruteEnemies; i++)
			{
				getline(readSaveFile, loadedVariable);
				boolState = atoi(loadedVariable.c_str());

				getline(readSaveFile, loadedVariable);
				xPos = atof(loadedVariable.c_str());

				getline(readSaveFile, loadedVariable);
				yPos = atof(loadedVariable.c_str());
			
				bruteEnemies[i]->changeBoolVisible(boolState);
				bruteEnemies[i]->updatePosition(xPos, yPos);
			}

			 // getting in bruteEnemies states , health and position from the file and then setting them
			getline(readSaveFile, loadedVariable);
			boolState = atoi(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			xPos = atof(loadedVariable.c_str());

			getline(readSaveFile, loadedVariable);
			yPos = atof(loadedVariable.c_str());
			
			raiderEnemies->changeBoolVisible(boolState);
			raiderEnemies->updatePosition(xPos, yPos);
	}
	else std::cout << "Unable to open file";

	// closing the file so other streams can get access to it and the file doesn't get accidently damaged
	readSaveFile.close();
	
}

// function for setting specific things within mapOne such as player start point, enemy positions and visibility states
void StatePlay::mapOne()
{
	  //updating the positions and reseting the variables for Player, NPCs and items
	for(int i = 0; i < numberOfFodderEnemies; i++)
	{
		fodderEnemies[i]->updatePosition((float)rand()/RAND_MAX - 0.75f, (float)rand()/RAND_MAX - 0.75f);
		fodderEnemies[i]->resetVariables();
	}
	
	for(int i = 0; i < numberOfBruteEnemies; i++)
	{
		bruteEnemies[i]->updatePosition((float)rand()/RAND_MAX - 0.75f, (float)rand()/RAND_MAX - 0.75f);
		bruteEnemies[i]->resetVariables();
	}

	raiderEnemies->updatePosition((float)rand()/RAND_MAX - 0.75f, (float)rand()/RAND_MAX - 0.75f);
	raiderEnemies->resetVariables();

	for(int i = 0; i < numberOfHealthPacks; i++)
	{
		healthPack[i]->updatePosition((float)rand()/RAND_MAX - 0.75f, (float)rand()/RAND_MAX - 0.75f);
	 	healthPack[i]->resetVariables();
	}

	for(int i = 0; i < numberOfCombatPacks; i++)
	{
		combatPack[i]->updatePosition((float)rand()/RAND_MAX - 0.75f, (float)rand()/RAND_MAX - 0.75f);
		combatPack[i]->resetVariables();
	}

	stimulant->updatePosition((float)rand()/RAND_MAX - 0.75f, (float)rand()/RAND_MAX - 0.75f);
	stimulant->resetVariables();

	player->updatePosition(0.9f, 0.7f);
	player->resetVariables();
}

// statePlay destructor deleting various objects that get instantiated within this class
StatePlay::~StatePlay()
{
	// deleting any objects I instantiated within this class
	delete playerStatLabel;
	delete player;
	delete raiderEnemies;
	delete stimulant;

	for(int i = 0; i < numberOfBruteEnemies; i++)
	{
		delete bruteEnemies[i];
	}

	for(int i = 0; i < numberOfFodderEnemies; i++)
	{
		delete fodderEnemies[i];
	}

	for(int i = 0; i < numberOfCombatPacks; i++)
	{
		delete combatPack[i];
	}

	for(int i = 0; i < numberOfHealthPacks; i++)
	{
		delete healthPack[i];
	}
}