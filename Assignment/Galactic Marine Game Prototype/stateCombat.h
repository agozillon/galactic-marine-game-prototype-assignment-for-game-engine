#ifndef STATECOMBAT_H
#define STATECOMBAT_H
#include "gameState.h"   // including gameState so CombatState can inherit from it, also including AbstractPC/NPC so I can use it within functions, including label so I can use labels within this class
#include "abstractNPC.h"
#include "abstractPC.h"
#include "label.h"

// as you can see below StateCombat is a culmination of all of the inherited Abstract classes functions with some extension
class StateCombat: public GameState{

public:
	StateCombat();                                  // StateCombatSelect constructor
    void draw();                                    // Function that is used to draw collections of other objects during this state
    void init();                                    // Function that is used to initilize all of the things that are required to run this state(instead of using the constructor)
    void update();                                  // Function that is used to update this state 
    void handleSDLEvent(SDL_Event const &sdlEvent); // Function that is used to handleSDLEvents, all keyboard events at the moment
    void exit();                                    // Function that should be called when the state is switching, anything you wish to happen when you exit this state should go in this function
    void enter();                                   // Function that should be called when the state is switching, anything you wish to happen when you enter this state should go in this function
	~StateCombat();                                 // StateCombatSelect destructor
	
private:
	void combatResolution(AbstractNPC * enemy, AbstractPC * player);        // Function that is for Normal Combat Resolution in which an PC and NPC are passed in as a parameter and are matched up against each other randomizing is not part of this Combat Function
	void randomCombatResolution(AbstractNPC * enemy, AbstractPC * player);  // Function that is for Random Combat Resolution in which an PC and NPC are passed in as a parameter and are matched up against each other randomizing is a part of the combat in this function
	void itemDrop(AbstractPC * player);                                     // Function that when called randomly selects one of the 3 items and adds the stats on to the player!
	
	// variables and/or object pointers that StateCombat requires the identifiers are descriptive
	Label * enemyStatsLabel;
	Label * playerStatsLabel;
	Label * playerWonLabel;
	Label * enemyWonLabel;
	
	bool combatHasBeenResolved;
	bool enemyWon;
	bool playerWon;
};

#endif