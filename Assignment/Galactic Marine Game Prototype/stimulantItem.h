#ifndef STIMULANTITEM_H
#define STIMULANTITEM_H
#include "abstractItem.h" // including abstractItem so that StimulantItem can inherit from it and gain libraries from it

// as you can see below StimulantItem is a culmination of all of the inherited Abstract classes functions
class StimulantItem: public AbstractItem{

public:
	StimulantItem();                                       // StimulantItem constructor
	void drawLabel(float x, float y);                      // function for taking in two float variables and then drawing the label at that position 
	void draw();                                           // function for calling the relevant draw functions to draw the object on screen
	void updatePosition(float xPos, float yPos);           // function for updating the objects position (just the rectangle in this case)
	int getStatIncrease(){return speedIncrease;}	       // inline function that returns the StimulantItem's health increase
	void resetVariables();                                 // function for reseting the variables of StimulantItem (mostly the visible boolean but in future there could be more) 
	bool getStateOfVisibility(){return visible;}            // inline function that returns the state of the visible boolean
	void changeBoolVisible(bool change){visible = change;} // function for changing the visible boolean
	~StimulantItem();                                      // StimulantItem destructor

private:
	// variables and/or object pointers that StimulantItem requires the identifiers are descriptive
	int speedIncrease;
	bool visible;	
};

#endif