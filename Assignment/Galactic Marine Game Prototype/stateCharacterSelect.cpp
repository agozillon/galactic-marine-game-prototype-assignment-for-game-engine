#include "stateCharacterSelect.h" // including the stateCharacterSelect header so I can code implementations of the functions
#include "game.h"                 // including game so I can call various Game functions (like getSDLWindow, or state related functions)

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StateCharacterSelect::StateCharacterSelect()
{
	init();
}

// function to initilize the various objects and variables within this state
void StateCharacterSelect::init()
{
	// creating Labels by passing in the text,SDL color, size and font
	SDL_Color colour = { 255, 255, 0 }; // creating an SDL Color 
	characterSelectLabel = new Label("Character Select", TTF_OpenFont("MavenPro-Regular.ttf", 46),  colour);
	pressKeyToContinueLabel = new Label("Press return key to continue", TTF_OpenFont("MavenPro-Regular.ttf", 24),  colour);
}

// function to draw all of the various things that are required to be drawn during this state
void StateCharacterSelect::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
  
	// drawing labels to screen at the passed in positions
	characterSelectLabel->draw(-0.3, 0);
	pressKeyToContinueLabel->draw(-0.26, -0.1);

	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StateCharacterSelect::update()
{
	// calling this stages draw function
	draw();
}

// function for handling various keyboard input and changing/effecting the current state depending on choice
void StateCharacterSelect::handleSDLEvent(SDL_Event const &sdlEvent)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{
			
			// if return is press set state to playState
			case SDLK_RETURN:
				Game::getInstance()->setState(Game::getInstance()->getPlayState());
				break;
			
				// if anything else is pressed then don't do anything
			default:
				break;

		}
	}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StateCharacterSelect::exit()
{
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StateCharacterSelect::enter()
{
}

// stateCharacterSelect destructor deleting various objects that get instantiated within this class
StateCharacterSelect::~StateCharacterSelect()
{
	// deleting the labels created within this class
	delete characterSelectLabel;
	delete pressKeyToContinueLabel;
}