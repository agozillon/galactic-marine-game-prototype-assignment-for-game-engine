#ifndef ABSTRACTNPC_H
#define ABSTRACTNPC_H
#include "abstractCharacter.h"  // including the abstractCharacter header so AbstractNPC can inherit from it

// Abstract base class for NPC's encase they have seperate functions from PC(player character): no implementation as it is abstract 
//and it inherits from AbstractInGameObject as it will be an in game object (thus needs draw etc.). this class is specificly for in 
//game NPCs and thus all in game NPCS will require these functions. (it's currently quite blank but it leaves NPCs open for extension
//in the future)
class AbstractNPC: public AbstractCharacter{
// all of the functions below are pure virtual functions so any class that inherits will need to implement its own or else be classed as abstract
public:
	virtual int getMoneyDrop() = 0;                          // function to get the amount of money that each NPC will drop
	virtual int getDropRate() = 0;                           // function to get the drop rate of items for each NPC
	virtual ~AbstractNPC(){}                                 // blank deconstructor
};

#endif