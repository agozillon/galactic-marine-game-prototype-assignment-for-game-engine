#ifndef LABEL_H
#define LABEL_H
#include <SDL.h> // including the various SDL/GL libraries so that I can use various SDL and GL types and functions
#include <SDL_ttf.h>
#include <GL/glew.h>

// as you can see below Label is similar to your class except I extended it slightly for my use
class Label{

public:
	Label(const char * str, TTF_Font *font, SDL_Color colour);                                                 // Constructor that takes in parameters and uses them in the textToTexture function
	Label();                                                                                                   // Blank constructor for use with my void textToTexture
	static GLuint textToTexture(const char * str, TTF_Font* textFont, SDL_Color colour, GLuint &w, GLuint &h); // Function that returns a texture so that it can be drawn within the Draw function(used in conjunction with the constructor that takes in parameters)
	void textToTexture(const char * str, TTF_Font* textFont);                                                  // Function that is used with the blank constructor in cases where we want to pass in what we want to use as text further on in the code than on initilizaton (also uses a default colour)
	void draw(GLfloat x, GLfloat y);                                                                           // Function that draws the current texture to screen
	GLuint getWidth(){return width;}                                                                           // Function that returns the object width
	GLuint getHeight(){return height;}                                                                         // Function that returns the object height
	GLuint getTextID(){return textID;}                                                                         // Function that returns the object textID
	
private:
	// variables and/or object pointers that Rect requires the identifiers are descriptive
	GLuint textID;
	GLuint height;
	GLuint width;
};

#endif