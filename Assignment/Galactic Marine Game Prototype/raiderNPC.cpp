#include "raiderNPC.h" // including raiderNPC header file so I can create implementations for some of its functions

// assigning values and creating objects within the RaiderNPC constructor
RaiderNPC::RaiderNPC():maxHealth(12), strength(7), speed(12), moneyDrop(100), dropRate(40), visible(true)
{	
	// passing in the NPC marker Color and Creating New Rect Object
	marker = new Rect(1.0f, 0.0f, 0.0f);

	//set the color, text and size of the label
	SDL_Color colour = { 255, 255, 0 };
	name = new Label("Raider",TTF_OpenFont("MavenPro-Regular.ttf", 24),colour);
}

// used to update/change the position of the Rect object called marker by passing in parameters for x and y
void RaiderNPC::updatePosition(float xPos, float yPos)
{
	//setting the marker position
	marker->setX(xPos);
	marker->setY(yPos);
}

//used to change the RaiderNPCs base stats(strength, speed, health) by passing in values  
void RaiderNPC::changeBaseStats(int str, int sp, int hp)
{
	strength += str;
	speed += sp;
	maxHealth += hp;
}

// used to reset the RaiderNPCs variables to there default state
void RaiderNPC::resetVariables()
{
	visible = true;
	maxHealth = 12;
	strength = 7;
	speed = 12;
	moneyDrop = 100;
	dropRate = 40;
}

// used to draw the parts of BruteNPC to screen that are required(could for instance also draw the label to screen at the marker position)
void RaiderNPC::draw()
{	
	// calling the marker(Rect) draw function
	marker->draw();
}

// for drawing the RaiderNPC's name(label) to screen pass in parameters for where you wish it to be drawn
void RaiderNPC::drawLabel(float x, float y)
{
	// calling the name(Label) draw function 
	name->draw(x, y);
}

// RaiderNPC deconstructor delete objects and do other things that are required for the Deletion of RaiderNPC
RaiderNPC::~RaiderNPC()
{
	// delete the objects instantiated
	delete name;
	delete marker;
}