#include "Label.h"
#include "game.h"

// constructor that uses passed in parameters to set up a textID using the textToTexture method that returns a GLuint
Label::Label(const char * str, TTF_Font *font, SDL_Color colour)
{
	textID = textToTexture(str, font, colour, width, height);
}

//default blank constructor for the second textToTexture method
Label::Label()
{
}

// function that returns a texture that can be drawn to screen, accepts color, font, width, height and text
GLuint Label::textToTexture(const char * str, TTF_Font* font, SDL_Color colour, GLuint &w, GLuint &h)
{
	SDL_Surface *stringImage;                                     // pointer to an SDL_Surface
	stringImage = TTF_RenderText_Blended(font, str, colour);      // setting up the SDL_Surface using parameters passed in
	
	// if the stringImage pointer is null then call the exitFatalError function and pass in this parameter
	if (stringImage == NULL)
		Game::getInstance()->exitFatalError("String surface not created");

	w = stringImage->w; // set GLuint width to stringImage width
	h = stringImage->h; // set GLuint height to stringImage height
	GLuint colours = stringImage->format->BytesPerPixel; // set GLuint colours to the stringImage format

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	// generating and setting up the texture
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);

	// return the current texture
	return texture;
}

// another way to create a texture in the case that you use a blank constructor
// this is to get around cases in which you may have to declare an isntance of the class within
// a recurring method or if you want to use default values for color(tried to comment inside the function as
// best I can)
void Label::textToTexture(const char * str, TTF_Font* textFont)
{
	SDL_Surface *stringImage;                                     // pointer to an SDL_Surface
	SDL_Color colour = { 255, 255, 0 };                           // creating an SDL colour
	stringImage = TTF_RenderText_Blended(textFont, str, colour);  // setting up the SDL_Surface using parameters passed in

	// if the stringImage pointer is null then call the exitFatalError function and pass in this parameter
	if (stringImage == NULL)
		Game::getInstance()->exitFatalError("String surface not created");

	height = stringImage->h; // set GLuint height to stringImage height
	width = stringImage->w;  // set GLuint width to stringImage width
	GLuint colours = stringImage->format->BytesPerPixel; // set GLuint colours to the stringImage format

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	// generating and setting up the texture
	GLuint texture;
	glDeleteTextures(1, &textID); // deleting textIDs previous texture(just in case)
	glDeleteTextures(1, &texture); // deleting texture even though it just got 
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, stringImage->w, stringImage->h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	
	textID = texture;
	
	glDisable(GL_TEXTURE_2D); // function to disable the current GL texture

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage); 
}

// function that takes in parameters and uses the current texture to draw the label to screen
void Label::draw(GLfloat x, GLfloat y)
{
	// Draw texture here
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, textID);
    glBegin(GL_QUADS);
	  glTexCoord2d(0,1); // Texture has origin at top not bottom
      glVertex2f (x,y); // first corner
	  glTexCoord2d(1,1);
	  glVertex2f (x+0.002f*width, y); // second corner
	  glTexCoord2d(1,0);
      glVertex2f (x+0.002f*width, y+0.002f*height); // third corner
	  glTexCoord2d(0,0);
      glVertex2f (x, y+0.002f*height); // fourth corner
    glEnd();

	glDisable(GL_TEXTURE_2D); // function to disable the current GL texture
}




