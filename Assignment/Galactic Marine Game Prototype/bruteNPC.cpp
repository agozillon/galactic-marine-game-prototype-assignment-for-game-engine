#include "bruteNPC.h" // including bruteNPC header file so I can create implementations for some of its functions

// assigning values and creating objects within the BruteNPC constructor
BruteNPC::BruteNPC(): maxHealth(12), strength(8), speed(6), moneyDrop(10), dropRate(15), visible(true)
{	
	// passing in the NPC marker Color and Creating New Rect Object(it will be of default size)
	marker = new Rect(1.0f, 1.0f, 0.0f);

	//set the color, text and size of the label
	SDL_Color colour = { 255, 255, 0 };
	name = new Label("Brute",TTF_OpenFont("MavenPro-Regular.ttf", 24),colour);
}

// used to update/change the position of the Rect object called marker by passing in parameters for x and y
void BruteNPC::updatePosition(float xPos, float yPos)
{
	//setting the marker position
	marker->setX(xPos);
	marker->setY(yPos);
}

//used to change the BruteNPCs base stats(strength, speed, health) by passing in values  
void BruteNPC::changeBaseStats(int str, int sp, int hp)
{
	strength += str;
	speed += sp;
	maxHealth += hp;
}

// used to reset the BruteNPCs variables to there default state
void BruteNPC::resetVariables()
{
	visible = true;
	maxHealth = 12;
	strength = 8;
	speed = 6;
	moneyDrop = 10;
	dropRate = 15;
}

// used to draw the parts of BruteNPC to screen that are required(could for instance also draw the label to screen at the marker position)
void BruteNPC::draw()
{	
	// calling the marker(Rect) draw function
	marker->draw();
}

// for drawing the BruteNPC's name(label) to screen pass in parameters for where you wish it to be drawn
void BruteNPC::drawLabel(float x, float y)
{
	// calling the name(Label) draw function 
	name->draw(x, y);
}

// BruteNPC deconstructor delete objects and do other things that are required for the Deletion of BruteNPC
BruteNPC::~BruteNPC()
{
	// delete the objects instantiated
	delete name;
	delete marker;
}