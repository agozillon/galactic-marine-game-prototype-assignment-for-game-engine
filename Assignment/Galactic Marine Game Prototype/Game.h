#pragma
#ifndef GAME_H
#define GAME_H
#include <SDL_ttf.h> // including SDL abstractNPC/PC Labels libraries so that I can use various functions and types within my class
#include <SDL.h>
#include "abstractNPC.h" 
#include "abstractPC.h"
#include "label.h"
#include "gameState.h" // including GameState so that I can create pointers and functions for the State objects
#include "statePlay.h"

// this game class contains the run time loop and all of the instantiated states and functions to switch states
// it also has a few other functions and variables that need to be used over multiple states and/or classes
// it is currently a singleton this is largely to simplify changing states and using the various other functions 
// without having to pass in the Game object all of the time, it's also a singleton as we only ever want 1 instantiated 
// game object at a time. 
class Game{

public:
	static Game *getInstance();                                                    // function that returns a pointer to a Game Object so we can use the functions from here(static so that its useable from anywhere without an instansiated Game object)  
	void run();                                                                    // function that contains the game run loop
	void setState(GameState * newState);                                           // function to change the current state to the passed in GameState
	GameState *getState(void){return currentState;}                                // inline function returns the currentState GameState pointer
	GameState *getPlayState(void){return playState;}                               // inline function returns the playState GameState pointer
	GameState *getMainMenuState(void){return mainMenuState;}                       // inline function returns the mainMenuState GameState pointer
	GameState *getCreditsState(void){return creditsState;}                         // inline function returns the creditsState GameState pointer
	GameState *getSplashScreenState(void){return splashScreenState;}               // inline function returns the splashScreenState GameState pointer
	GameState *getCharacterSelectState(void){return characterSelectState;}         // inline function returns the characterSelectState GameState pointer
	GameState *getGameOverState(void){return gameOverState;}                       // inline function returns the gameOverState GameState pointer
	GameState *getGameWonState(void){return gameWonState;}                         // inline function returns the gameWonState GameState pointer
	GameState *getCombatState(void){return combatState;}                           // inline function returns the combatState GameState pointer
	GameState *getPauseState(void){return pauseState;}                             // inline function returns the pauseState GameState pointer
	GameState *getExitPromptState(void){return exitPromptState;}                   // inline function returns the exitPromptState GameState pointer
	StatePlay *getCastedStatePlay(void);                                           // function that returns a pointer to PlayState tht has been casted to PlayState instead of GameState
	void setCurrentEnemy(AbstractNPC * enemy){currentEnemy = enemy;}               // inline function that passes in an AbstractNPC and sets the currentEnemy pointer to point to it
	void setCurrentPlayer(AbstractPC * player){currentPlayer = player;}            // inline function that passes in an AbstractPC and sets the currentEnemy pointer to point to it
	void changeNewGame(bool changed){newGame = changed;}                           // inline function that passes in boolean changed and then sets newGame to equal it
	bool getNewGameBool(){return newGame;}                                         // inline function that returns the newGame boolean
	void changeContinueGame(bool changed){continueGame = changed;}                 // inline function that passes in boolean changed and then sets continueGame to equal it
	bool getContinueGameBool(){return continueGame;}                               // inline function that returns continueGame
	void changeIsThereASavedGameBool(bool changed){isThereASavedGame = changed;}   // inline function that passes in boolean changed and then sets IsThereASavedGameBool to equal it
	bool getIsThereASavedGame(){return isThereASavedGame;}                         // inline function that returns the isThereASavedGame boolean
	void exitFatalError(char *message);                                            // function that outputs a console message and then quits the SDL window when called(used where SDL errors can occur)
	SDL_Window * getSDLWindow(){return window;}                                    // inlien function that returns the created SDL window
	AbstractNPC *getCurrentEnemy(void){return currentEnemy;}					   // inline function that returns the currentEnemy pointer
	AbstractPC *getCurrentPlayer(void){return currentPlayer;}                      // inline function that returns the currentPlayer pointer
	~Game();                                                                       // Game destructor

private:
	Game();                                                                // private Game constructor so that no other class can accidently create an Game object (Singleton so there should only ever be one!)
	Game(Game const&){};                                                   // creating my own copy constructor that is blank and private so that no copies can be created for my singleton
	SDL_Window * setupRC(SDL_GLContext &context);                          // creates an OpenGL context for use with SDL
	
	// variables and/or object pointers that Game requires the identifiers are descriptive
	// I shall explain the use for if some of the variables usage is not entirely clear
	TTF_Font* textFont;	                                                   // SDL type for True-Type font rendering
	static Game * instance;                                                // static Game pointer (for the singletons so there is only one of these)
	SDL_GLContext glContext;                                               // OpenGL context handle
	SDL_Window *window;                                                    // SDL_Window type to hold the SDL_Window return from setupRC
	AbstractNPC * currentEnemy;                                            // AbstractNPC pointer
	AbstractPC * currentPlayer;                                            // AbstractPC pointer
	GameState * currentState;                                              // GameState pointer for holding the various states
	GameState * playState;                                                 // GameState pointer for holding the various states
	GameState * mainMenuState;                                             // GameState pointer for holding the various states
	GameState * creditsState;                                              // GameState pointer for holding the various states
	GameState * characterSelectState;                                      // GameState pointer for holding the various states
	GameState * splashScreenState;                                         // GameState pointer for holding the various states
	GameState * gameOverState;                                             // GameState pointer for holding the various states
	GameState * gameWonState;                                              // GameState pointer for holding the various states
	GameState * combatState;                                               // GameState pointer for holding the various states
	GameState * pauseState;                                                // GameState pointer for holding the various states
	GameState * exitPromptState;                                           // GameState pointer for holding the various states
	StatePlay * downCastedPlayState;                                       // StatePlay pointer for pointing to StatePlay and using playState specific functions like load/save

	bool running;                                                          
	bool newGame;
	bool continueGame;
	bool isThereASavedGame;
};

#endif