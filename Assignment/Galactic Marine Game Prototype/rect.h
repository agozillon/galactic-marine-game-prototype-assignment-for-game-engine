#ifndef RECT_H
#define RECT_H
#pragma once

// class for drawing rectangles to screen used as the "model" for all of the objects in the game
class Rect{

public:
	Rect(float red, float green, float blue);                                          // constructor that uses defualt values for size and position but allows you to change the color
	Rect(float x, float y, float w, float h, float red, float green, float blue);      // constructor that takes in the width, height, and x and y positions as well as the color
	float getX(){return xPos;}                                                         // inline function that returns the xPos as an float
	float getY(){return yPos;}                                                         // inline function that returns the yPos as an float
	void setX(float x){xPos = x;}                                                      // inline function that sets xPos to the parameter passed in
	void setY(float y){yPos = y;}                                                      // inline function that sets yPos to the parameter passed in
	float getW(){return width;}                                                        // inline function that returns the width as a float
	float getH(){return height;}                                                       // inline function that returns the height as a float
	void setW(float w){width = w;}                                                     // inline function that sets the width to the parameter passed in
	void setH(float h){height = h;}                                                    // inline function that sets the height to the parameter passed in
	void draw(void);                                                                   // function that draws the rectanlge on to the screen
	~Rect(void);                                                                       // rect destructor

private:
	// variables and/or object pointers that Rect requires the identifiers are descriptive
	// I shall explain the use for some of the ones thats usage may seem slightly unusual
	float xPos;
	float yPos;
	float width;
	float height;
	float R; // Red
	float G; // Green
	float B; // Blue RGB Color Scale
};

#endif