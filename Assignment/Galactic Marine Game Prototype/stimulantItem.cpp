#include "stimulantItem.h"  // including StimulantItem header file so I can create implementations for some of its functions

// assigning values and creating objects within the StimulantItem constructor
StimulantItem::StimulantItem(): speedIncrease(1), visible(true)
{	
	// passing in the Color parameters as well as setting the size while Creating this New Rect Object
	marker = new Rect(0, 0, 0.05, 0.05, 0.0f, 1.0f, 0.0f);

	//set the color, text and size of the label
	SDL_Color colour = { 255, 255, 0 };
	name = new Label("Stimulant ",TTF_OpenFont("MavenPro-Regular.ttf", 24),colour);
}

// used to update/change the position of the Rect object called marker by passing in parameters for x and y
void StimulantItem::updatePosition(float xPos, float yPos)
{
	//setting the marker position
	marker->setX(xPos);
	marker->setY(yPos);
}

// used to reset the StimulantItems variables to there default state
void StimulantItem::resetVariables()
{
	visible = true;
}

// used to draw the parts of StimulantItem to screen that are required
void StimulantItem::draw()
{	
	// calling the marker(Rect) draw function
	marker->draw();
	
	// draw name(label) and position it on the marker using the object drawLabel function instead of going 
	// directly through the label draw function
	drawLabel(marker->getX()+(marker->getW()/2.0f), marker->getY()+marker->getH());
}

// for drawing the StimulantItem's name(label) to screen pass in parameters for where you wish it to be drawn
void StimulantItem::drawLabel(float x, float y)
{
	// calling the name(Label) draw function 
	name->draw(x, y);
}

// StimulantItem deconstructor delete objects and do other things that are required for the Deletion of StimulantItem
StimulantItem::~StimulantItem()
{
	// delete the objects instantiated
	delete name;
	delete marker;
}